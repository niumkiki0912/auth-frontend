import axios from 'axios'
import store from '@/store'
import storage from 'store'
import message from 'ant-design-vue/es/message'
import { VueAxios } from './axios'
import { ACCESS_TOKEN } from '@/store/mutation-types'

// 创建 axios 实例
const request = axios.create({
  // API 请求的默认前缀
  baseURL: process.env.VUE_APP_API_BASE_URL,
  timeout: 6000 // 请求超时时间
})

// 异常拦截处理器
const errorHandler = (error) => {
  if (error.response) {
    const data = error.response.data
    // 从 localstorage 获取 token
    const token = storage.get(ACCESS_TOKEN)
    if (error.response.status === 403) {
      message.error('请求失败，禁止访问')
    }
    if (error.response.status === 401 && !(data.data && data.data.isLogin)) {
      message.error('未授权请求，请登录')
      if (token) {
        store.dispatch('Logout').then(() => {
          setTimeout(() => {
            window.location.reload()
          }, 1500)
        })
      }
    }
  }
  return Promise.reject(error)
}

// request interceptor
request.interceptors.request.use(config => {
  const token = storage.get(ACCESS_TOKEN)
  // 如果 token 存在
  // 让每个请求携带自定义 token 请根据实际情况自行修改
  if (token) {
    config.headers['X-API-TOKEN'] = token
  }
  return config
}, errorHandler)

// response interceptor
request.interceptors.response.use((response) => {
  // 登录已失效
  // if (response.data.code === 401) {
  //   const token = storage.get(ACCESS_TOKEN)
  //   if (token) {
  //     store.dispatch('Logout').then(() => {
  //       setTimeout(() => {
  //         window.location.reload()
  //       }, 1500)
  //     })
  //   }
  // }
  if (response.data.code > 0) {
    message.error(response.data.message)
    // window.$message.success(response.data.message)
  }
  return response.data.data
}, errorHandler)

const installer = {
  vm: {},
  install (Vue) {
    Vue.use(VueAxios, request)
  }
}

export default request

export {
  installer as VueAxios,
  request as axios
}
