import request from '@/utils/request'

const api = {
  applicationList: '/omp/application/list',
  mnuList: '/omp/menu/search',
  updateOrCreate: '/omp/application/updateOrCreate'
}

export default api
export function getMenuList (parameter) {
  return request({
    url: api.mnuList,
    method: 'GET',
    params: parameter
  })
}
// id == 0 add     post
// id != 0 update  put
export function saveService (parameter) {
  return request({
    url: api.service,
    method: parameter.id === 0 ? 'post' : 'put',
    data: parameter
  })
}
