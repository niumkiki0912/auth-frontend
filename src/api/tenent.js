import request from '@/utils/request'

const api = {
  tenantList: '/omp/tenant/getList',
  addTenant: '/omp/tenant/addTenant',
  editTenant: '/omp/tenant/editTenant',
  getDetail: '/omp/tenant/getDetail',
  getStoreList: '/omp/store/getList',
  getRoleList: '/omp/role/getList',
  updateOrCreateRole: '/omp/role/updateOrCreate',
  roleDetail: '/omp/role/detail',
  getUserList: '/omp/user/getList',
  createUser: '/omp/user/create',
  updateUser: '/omp/user/update',
  userDetail: '/omp/user/detail',
  getAuthMenus: '/omp/tenant/getAuthMenus',
  authMenus: '/omp/tenant/authMenus'
}

export default api
export function getTenantList (parameter) {
  return request({
    url: api.tenantList,
    method: 'GET',
    params: parameter
  })
}

export function addTenant (parameter) {
  return request({
    url: api.addTenant,
    method: 'POST',
    data: parameter
  })
}

export function editTenant (parameter) {
  return request({
    url: api.editTenant,
    method: 'POST',
    params: parameter
  })
}

export function getDetail (parameter) {
  return request({
    url: api.getDetail,
    method: 'GET',
    params: parameter
  })
}

// 授权
export function authMenus (parameter) {
  return request({
    url: api.authMenus,
    method: 'POST',
    data: parameter
  })
}

// 租户授权平台详情
export function getAuthMenus (parameter) {
  return request({
    url: api.getAuthMenus,
    method: 'GET',
    params: parameter
  })
}

// 获取门店列表
export function getStoreList (parameter) {
  return request({
    url: api.getStoreList,
    method: 'GET',
    params: parameter
  })
}

export function getRoleList (parameter) {
  return request({
    url: api.getRoleList,
    method: 'GET',
    params: parameter
  })
}

// 创建角色
export function updateOrCreateRole (parameter) {
  return request({
    url: api.updateOrCreateRole,
    method: 'POST',
    data: parameter
  })
}

// 角色详情
export function roleDetail (parameter) {
  return request({
    url: api.roleDetail,
    method: 'GET',
    params: parameter
  })
}

// 获取用户列表
export function getUserList (parameter) {
  return request({
    url: api.getUserList,
    method: 'GET',
    params: parameter
  })
}

// 创建用户
export function createUser (parameter) {
  return request({
    url: api.createUser,
    method: 'POST',
    data: parameter
  })
}

// 用户详情
export function userDetail (parameter) {
  return request({
    url: api.userDetail,
    method: 'GET',
    params: parameter
  })
}

// 更新用户
export function updateUser (parameter) {
  return request({
    url: api.updateUser,
    method: 'POST',
    data: parameter
  })
}
