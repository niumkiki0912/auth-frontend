import request from '@/utils/request'

const api = {
  applicationList: '/omp/application/list',
  mnuList: '/omp/menu/search',
  updateOrCreate: '/omp/application/updateOrCreate',
  getMenuList: 'omp/menu/getList',
  addMenu: '/omp/menu/addMenu',
  editMenu: '/omp/menu/editMenu',
  delMenu: '/omp/menu/delMenu'
}

export default api

export function getApplicationList (parameter) {
  return request({
    url: api.applicationList,
    method: 'GET',
    params: parameter
  })
}
export function updateOrCreateApplication (parameter) {
  return request({
    url: api.updateOrCreate,
    method: 'POST',
    data: parameter
  })
}

export function getMenuList (parameter) {
  return request({
    url: api.getMenuList,
    method: 'GET',
    params: parameter
  })
}

export function addMenu (parameter) {
  return request({
    url: api.addMenu,
    method: 'post',
    data: parameter
  })
}

export function editMenu (parameter) {
  return request({
    url: api.editMenu,
    method: 'post',
    data: parameter
  })
}

export function delMenu (parameter) {
  return request({
    url: api.delMenu,
    method: 'post',
    data: parameter
  })
}
