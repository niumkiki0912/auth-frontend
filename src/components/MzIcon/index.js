
import { Icon } from 'ant-design-vue'

export default Icon.createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_2453794_94hfs2l1a8.js'
})
