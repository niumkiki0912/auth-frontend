
import { Icon } from 'ant-design-vue'

export default Icon.createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_2540429_2ia534mzwe7.js'
})
